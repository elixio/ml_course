MAIN = main
TEX_SOURCES = Makefile $(wildcard *.tex)
FIGURES := $(shell find * -type f)
SHELL = /bin/bash
COVER = title.tex
OPT = --interaction=nonstopmode
PROJECT = 'cours'
DATE = $(shell date +"%d%b%Y")
FLAG = -quiet

.PHONY : all compilation pdf glossaire clean update targz

.DEFAULT:
	$(info Cible $(TARGET) inconnue)
	@make -s help


# pdflatex $(MAIN)
# makeindex $(MAIN)
# biber $(MAIN)
# pdflatex $(MAIN)
# pdflatex $(MAIN)
# latexmk -pdf -pvc -pdflatex="pdflatex $(OPT)" $(MAIN)


all: $(MAIN) ## clean, compilation, clean puis ouverture avec firefox
	@$(MAKE) clean
	@$(MAKE) pdf
	@$(MAKE) pdf
	@$(MAKE) clean
	@firefox $<.pdf&


compilation : $(MAIN) ## génère le fichier pdf (compile, fais le glossaire et recompile)
	$(MAKE) pdf
	$(MAKE) glossaire
	$(MAKE) pdf


pdf : $(MAIN)
	pdflatex $(FLAG) $<.tex


$(MAIN).pdf : $(MAIN) $(TEX_SOURCES) $(FIGURES)
	@latexmk $(FLAG) -pdf $<.tex


# final.pdf : $(COVER) $(MAIN).pdf
# 	pdftk cover $(MAIN).pdf cat output $@


glossaire : $(MAIN)
	makeglossaries $<


clean : ## supprime les fichiers générés par la compilation
	@echo -e "suppression des fichiers de compilation"
	@rm -f $(MAIN).{log,aux,out,bbl,blg,toc,idx,bcf,run.xml,ind,ilg,fls,fdb_latexmk,glsdefs,acn,acr,alg,mw}
	@echo -e "suppression des fichiers de glossaire"
	@rm -f $(MAIN).{dvi,ist,gls,glg,glo,v[0-9],v[0-9]o,v[0-9]n,vgraphe,via,valgo,vuml,vos,vweb,vres,ven,vrsi}
	@echo -e "suppression des autres fichiers"
	@rm -f texput.{fls,log}


update : ## fais les modifications git (diff, commit, push puis status)
	$(info Modifs git :)
	@git diff --stat
	@read -p "Entrez le commentaire de la modification : " commentaire; \
	git commit -m "$$commentaire" -a
	@git push
	@git status


targz : ## compresse le dossier
	$(MAKE) clean
	$(MAKE) compilation
	$(MAKE) clean
	tar -cvzf $(PROJECT)_$(DATE).tgz $(TEX_SOURCES) $(SOURCES) cover.pdf


.PHONY:help # pour ne pas confondre help et le fichier help (même si il n'y en a pas)
help : ## affiche cette aide
		@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "} \
		{printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
# voir : self documentating makefile



