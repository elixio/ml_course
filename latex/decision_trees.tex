
\noindent
\begin{minipage}[c]{0.55\textwidth}
Decision trees predict labels for instances by traversing from
a root node to a leaf, using simple rules based on the instance's features.
They offer clear, interpretable models suitable for
binary classification among other tasks, with each leaf
representing a classification outcome determined by
a path of decisions from the root.
\end{minipage}
\hfill
\begin{minipage}[c]{0.4\textwidth}
    \centering
    \includegraphics[width=1.2\textwidth,valign=c]{decision_tree.png}
\end{minipage}

\subsection{Key Concepts}
\vspace{10px}

\textbf{Sample Complexity}

\begin{itemize}
\item Decision trees can split the instance space into cells,
with each leaf corresponding to a cell. Trees with arbitrary sizes
can lead to overfitting due to their potential to have
an infinite VC dimension.
\item To prevent overfitting, one strategy involves
limiting the size of the tree using Minimum Description Length (MDF),
balancing the trade-off between fitting the training data and
maintaining a manageable tree size.
\end{itemize}

\textbf{Decision Tree Algorithms}

\begin{itemize}
\item \textbf{ID3 Algorithm}: Constructs trees by recursively selecting
the feature that maximizes a gain measure (e.g., information gain)
for splitting, stopping when all instances in a subset belong to
the same class or when no more features are available.
\item \textbf{Gain Measures}: Strategies to evaluate the benefit of a
split include training error decrease, information gain (entropy reduction),
and Gini index.
\end{itemize}

\begin{itemize}
\item \textbf{Pruning}: To further mitigate overfitting,
trees can be pruned after construction.
Pruning reduces the size of the tree by replacing
nodes with leaves or smaller subtrees based on their estimated impact
on generalization error.
\end{itemize}

\begin{itemize}
\item \textbf{Real-Valued Features}: For real-valued features,
threshold-based splitting rules (e.g., decision stumps) are used.
The ID3 algorithm can be adapted for real-valued features by considering
binary features derived from thresholding the real-valued features.
\end{itemize}

\begin{itemize}
\item \textbf{Random Forests}: A method to enhance decision tree performance
is through the construction of a random forest, which consists
of multiple decision trees each built from a random subset of
the training data and a random subset of the features.
The final classification is determined by majority voting among the trees.
\end{itemize}

\vspace{20px}
\subsection{Important Formulas and Theorems}
\vspace{10px}

\begin{itemize}
\item \textbf{MDL Principle for Decision Trees}: Decision trees
are optimized not only for accuracy (fit to the data)
but also for simplicity, following the Minimum Description Length principle.
This approach prefers smaller trees that still capture
the essential patterns in the data.
\end{itemize}

\begin{itemize}
\item \textbf{Pruning Criterion}: A common criterion
for pruning is a cost-complexity measure, where the goal
is to minimize a combination of the tree's prediction error
on the training set and a penalty for the tree's complexity
(often measured as the number of leaves).
\end{itemize}




