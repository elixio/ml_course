
\hypertarget{overview}{%
\subsection{Overview}\label{overview}}

Chapter 14 delves into Stochastic Gradient Descent (SGD), an iterative
optimization method for minimizing the risk function directly,
particularly suitable for convex learning problems. Unlike Empirical
Risk Minimization (ERM) that relies on the empirical risk, SGD
iteratively updates hypotheses by taking steps in random directions
that, on average, align with the negative gradient of the risk function.

\hypertarget{key-concepts}{%
\subsection{Key Concepts}\label{key-concepts}}

\hypertarget{gradient-descent-gd}{%
\subsubsection{Gradient Descent (GD)}\label{gradient-descent-gd}}

\begin{itemize}
\item
  \textbf{Gradient Descent} is an optimization method for differentiable
  convex functions where updates are made in the direction opposite to
  the gradient to minimize the function.
\item
  \textbf{Update Rule}: \(w^{(t+1)} = w^{(t)} - \eta\nabla f(w^{(t)})\),
  where \(\eta\) > 0 is the learning rate.
\end{itemize}

\hypertarget{stochastic-gradient-descent-sgd}{%
\subsubsection{Stochastic Gradient Descent
(SGD)}\label{stochastic-gradient-descent-sgd}}

\begin{itemize}
\item
  \textbf{SGD} improves upon GD by using an estimated gradient
  (subgradient for nondifferentiable functions) calculated from a
  randomly selected sample, allowing direct minimization of the risk
  function.
\item
  \textbf{SGD Update Rule}: \(w^{(t+1)} = w^{(t)} - \eta v_t\), with
  \(v_t\) being a random vector whose expectation approximates the true
  gradient.
\item \textbf{Analysis of SGD}
  SGD can achieve similar convergence rates to GD, with the additional
  benefit of being applicable in scenarios where GD cannot be used due
  to computational constraints.
\end{itemize}

\hypertarget{sgd-for-learning-problems}{%
\subsubsection{SGD for Learning
Problems}\label{sgd-for-learning-problems}}

\begin{itemize}
\item
  \textbf{Direct Risk Minimization}: SGD facilitates direct minimization
  of the risk function \(L_D(w)\) by using random estimates of its
  gradient, thus circumventing the need for knowing the underlying
  distribution \(D\).
\item
  \textbf{Sample Complexity}: For convex-Lipschitz-bounded learning
  problems, SGD requires a number of iterations proportional to
  \(\frac{B^2\rho^2}{\epsilon^2}\) to achieve a risk within \(\epsilon\)
  of the optimum, where \(B\) and \(\rho\) are bounds on the norm of the
  hypothesis and the Lipschitz constant, respectively.
\end{itemize}

\hypertarget{formulas-and-theorems}{%
\subsection{Formulas and Theorems}\label{formulas-and-theorems}}

\begin{itemize}
\item \textbf{Convergence of SGD}
  For convex, Lipschitz functions with a suitable step size, SGD's
  output \(\bar{w}\) satisfies:
  \(E[f(\bar{w})] - f(w^*) \leq \frac{B \rho}{\sqrt{T}}\)\emph{, where
  \(w^*\)} is the optimum, \(B\) is a bound on \(||w^*||\), \(\rho\) is
  the Lipschitz constant, and \(T\) is the number of iterations. To achive
  $E[f(\bar{w})] - f(w^*) \leq \epsilon$, $T \geq \frac{B^2\rho^2}{\epsilon^2}$
\item \textbf{Strongly Convex Functions}
  SGD adapts well to strongly convex functions, offering improved
  convergence rates, especially when the function's strong convexity can
  be leveraged to fine-tune the algorithm's step size.
\end{itemize}

\hypertarget{applications-and-variants}{%
\subsection{Applications and Variants}\label{applications-and-variants}}

\begin{itemize}
\item
  \textbf{Projection Step}: Incorporating a projection step ensures the
  iterates remain within a specified convex set, enhancing SGD's
  applicability to constrained optimization problems.
\item
  \textbf{Variable Step Size and Averaging Techniques}: Modifying the
  step size over iterations or employing sophisticated averaging schemes
  of the iterates can lead to faster convergence or better practical
  performance.
\item
  \textbf{Strong Convexity}: Utilizing the strong convexity property of
  the objective function allows for more aggressive convergence
  guarantees.
\end{itemize}



