
\subsection{Infinite-Size Classes Can Be Learnable}

\begin{itemize}
\item Infinite hypothesis classes can still be PAC
(Probably Approximately Correct) learnable.
\item An example of such a class is the set
of threshold functions over the real line,
demonstrating that finiteness is not necessary for learnability.
\end{itemize}

\subsection{The VC-Dimension}

\define{VC-Dimension}{Vapnik-Chervonenkis dimension
of a hypothesis class is a measure of its complexity.
It represents the maximum number of points that can be shattered
(classified in all possible ways) by the class. Intuitively,
it reflects the class's capacity to fit arbitrary training data.}

VC-dimension provides a measure of the complexity of hypothesis
classes and is a key factor in determining their learnability.

\begin{theorem}
$\mathcal{H}$ is PAC learnable $\Leftrightarrow$ the class has a finite VC-dimension.\\
$\Leftarrow$ with uniform convergence property
\end{theorem}

\define{Sauer's Lemma (also known by Sauer-Shelah-Perles Lemma)}{
For a hypothesis class H with VC-dimension d, the number of
distinct functions that H can induce on any set of m > d samples is
bounded by a polynomial in m of degree d.}

\begin{itemize}
\item \textbf{Sample Complexity in relation to VC-simension}:
The sample complexity for learning a hypothesis class with
finite VC-dimension is determined by its VC-dimension.
This relationship indicates the number of samples needed to
learn the class to a given accuracy and confidence level.
\end{itemize}

\bluebox{The Fundamental Theorem of Statistical Learning}{Let H be a
hypothesis class of functions from a domain X to {0, 1} and let
the loss function be the 0 - 1 loss. Then, the following are equivalent:
\begin{enumerate}
\item H has the uniform convergence property.
\item Any ERM rule is a successful agnostic PAC learner for H.
\item H is agnostic PAC learnable.
\item H is PAC learnable.
\item Any ERM rule is a successful PAC learner for H.
\item H has a finite VC-dimension.
\end{enumerate}
}

\subsection{VC-dim of some functions}

Let's calculate the VC-dimension of several hypothesis classes.
To show that VCdim($\mathcal{H}$) = d we need to show that
\begin{enumerate}
\item There exists a set C of size d that is shattered by $\mathcal{H}$.
\item Every set C of size d + 1 is not shattered by $\mathcal{H}$.
\end{enumerate}

\begin{itemize}
\item \textbf{Threshold Functions}:
we have shown that for an arbitrary set C = {$c_1$},
$\mathcal{H}$ shatters C therefore $VCdim(\mathcal{H}) \geq 1$\\
we have also shown that for an arbitrary set C = {$c_1, c_2$}
where $c_1 \leq c_2$, $\mathcal{H}$ does not shatter C\\
Therefore $VCdim(\mathcal{H}) = 1$
\item \textbf{Intervals}:
Let $\mathcal{H} = \{h_{a,b} : a, b \in R, a < b\}$
where $h_{a,b} : \mathbb{R} \rightarrow \{0, 1\}$\\
Take C = {1, 2}, then $\mathcal{H}$ shatters C,
then VCdim($\mathcal{H}$) $\geq$ 2\\
now take C = {$c_1, c_2, c_3$} (we can assume
without loss of generality that $c1 \leq c2 \leq c3$)
The labeling (1, 0, 1) cannot be obtained so
$\mathcal{H}$ does not shatter C\\
Therefore $VCdim(\mathcal{H}) = 2$
\item \textbf{Axis Aligned Rectangles}:
Let $\mathcal{H}$ be the class of axis aligned rectangles, formally:
$\mathcal{H}$ = {$h_{(a_1,a_2,b_1,b_2)}$ : $a_1 \leq a_2$ and $b_1 \leq b_2$}
where $$h_{(a_1,a_2,b_1,b_2)}(x_1, x_2) =
\begin{cases}
&1 \text{ if } a_1 \leq x_1 \leq a_2 \text{ and } b_1 \leq x_2 \leq b_2\\
&0 \text{ otherwise }
\end{cases}$$\\
For that we need find a set of 4 points that are shattered by $\mathcal{H}$
end a set of 5 points that are not
\item \textbf{Finite Classes}:
For finite classes we have |$\mathcal{H}_C$ | $\leq$ |$\mathcal{H}$| and thus C
cannot be shattered if |$\mathcal{H}$| < $2^{|C|}$.
This implies that VCdim($\mathcal{H}$) $\leq log_2$(|$\mathcal{H}$|)\\
Note that VCdim(H) can be significantly smaller than $log_2$(|$\mathcal{H}$|)
\item \textbf{VC-Dimension and the Number of Parameters}:
The number of dimension is not always equal to the number
of parameters defining the hypothesis class.\\
$\mathcal{H}$ = {$h_{\theta} : \theta \in \mathbb{R}$} where $h_{\theta}$ :
$\mathcal{X} \rightarrow \{0, 1\}, h_{\theta}(x) = \lceil 0.5 sin(\theta x) \rceil$
\end{itemize}

\subsection{Formulas and Theorems}

\begin{itemize}
\item \textbf{Sauer-Shelah-Perles Lemma}:
$\tau_H(m) \leq \sum_{i=0}^{d} \binom{m}{i}$
Where $tau_H(m)$ is the growth function of H,
representing the maximal number of labelings of
m samples that can be realized by hypotheses in H.

\item \textbf{Fundamental Theorem of
Statistical Learning (Quantitative Version)}
If a hypothesis class H has a finite VC-dimension d,
then there exist constants C1 and C2 such that
for any $\epsilon > 0$ and $\delta > 0$,
the sample complexity m satisfies:
$$C_1 \frac{d + \log(1/\delta)}{\epsilon^2} \leq
m \leq C_2 \frac{d + \log(1/\delta)}{\epsilon^2}$$
\end{itemize}

\subsection{Important Definitions}

\begin{itemize}
\item \textbf{VC-dimension (VCdim(H))}: The maximal size of a set C that can be shattered by the hypothesis class H.
\item \textbf{Shattering}: A hypothesis class H shatters a set C if for every possible binary labeling of C, there exists a hypothesis in H that can realize such a labeling.
\item \textbf{Growth Function ($\tau_H(m)$)}: Represents the maximum number of labelings that can be induced by H on any set of m points.
\item \textbf{Uniform Convergence}: A property that ensures the empirical risk converges uniformly to the true risk for all hypotheses in the class, as the sample size increases.
\end{itemize}

\subsection{Applications}

\begin{itemize}
\item VC-dimension is a critical concept in understanding the theoretical underpinnings of machine learning models, especially in assessing their capacity to generalize from training data to unseen data.
\item It helps in guiding the choice of models based on their complexity and the available data size, aiming to balance underfitting and overfitting.
\end{itemize}





