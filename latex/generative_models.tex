
\hypertarget{overview}{%
\subsection{Overview}\label{overview}}

Generative models focus on modeling the distribution of data with
specific parametric forms, aiming to estimate the model's parameters.
Unlike discriminative approaches, which directly optimize prediction
accuracy, generative models provide insights into data structure,
supporting predictions and offering interpretability.

\hypertarget{key-concepts}{%
\subsection{Key Concepts}\label{key-concepts}}

\hypertarget{maximum-likelihood-estimation-mle}{%
\subsubsection{Maximum Likelihood Estimation
(MLE)}\label{maximum-likelihood-estimation-mle}}

\begin{itemize}
\item
  \textbf{Definition}: MLE seeks parameters that make the observed data
  most probable.
\item
  \textbf{Continuous Variables}: For continuous data, likelihood is
  defined using the probability density function.
\item
  \textbf{MLE as ERM}: MLE aligns with the Empirical Risk Minimization
  principle when using the log-loss function, treating parameter
  estimation as risk minimization.
\item
  \textbf{Log-Loss}: \(\ell(\theta, x) = -\log(P_\theta[x])\), where
  \(P_\theta\) is the probability of \(x\) given parameters \(\theta\).
\item
  \textbf{Generalization}: MLE's success hinges on the chosen model
  correctly representing the underlying data distribution. Incorrect
  assumptions can lead to poor generalization.
\end{itemize}

\hypertarget{naive-bayes}{%
\subsubsection{Naive Bayes}\label{naive-bayes}}

\begin{itemize}
\item
  Assumes feature independence given the label, simplifying
  \(P[X=x|Y=y]\) to a product of individual feature probabilities.
\item
  \textbf{Advantages}: Reduces parameter count from exponential to
  linear in the number of features, facilitating easier learning.
\end{itemize}

\hypertarget{linear-discriminant-analysis-lda}{%
\subsubsection{Linear Discriminant Analysis
(LDA)}\label{linear-discriminant-analysis-lda}}

\begin{itemize}
\item
  Assumes equal covariance matrices for all class-conditional densities
  and that these densities are Gaussian.
\item
  Results in a linear decision boundary defined by the mean vectors and
  the common covariance matrix.
\end{itemize}

\hypertarget{expectation-maximization-em}{%
\subsubsection{Expectation-Maximization
(EM)}\label{expectation-maximization-em}}

\begin{itemize}
\item
  \textbf{Purpose}: Efficiently computes MLE in the presence of latent
  variables, iteratively improving parameter estimates.
\item
  \textbf{E Step}: Updates estimates of latent variable distributions
  based on current parameters.
\item
  \textbf{M Step}: Optimizes parameters based on updated latent
  distributions.
\item
  Useful for models like Gaussian mixture models, where direct MLE is
  intractable.
\end{itemize}

\hypertarget{bayesian-reasoning}{%
\subsubsection{Bayesian Reasoning}\label{bayesian-reasoning}}

\begin{itemize}
\item
  \textbf{Approach}: Treats model parameters as random variables with
  their distributions, integrating prior knowledge through a prior
  distribution.
\item
  \textbf{Bayesian Prediction}: Adjusts predictions based on observed
  data, potentially incorporating prior beliefs about parameter
  distributions.
\item
  \textbf{Maximum A Posteriori (MAP)}: Estimates the most probable
  parameters given the data, balancing prior beliefs and observed data.
\end{itemize}

\hypertarget{practical-insights}{%
\subsection{Practical Insights}\label{practical-insights}}

\begin{itemize}
\item
  \textbf{Generative vs.~Discriminative}: Generative models offer deeper
  insights into data structure and are versatile in applications beyond
  classification. However, they may require stronger assumptions about
  data distribution.
\item
  \textbf{Choosing a Model}: The choice between Naive Bayes, LDA, and
  more complex models like Gaussian mixtures depends on the data
  structure and the assumptions one is willing to make.
\item
  \textbf{Handling Complexity}: Techniques like EM provide practical
  solutions for learning complex models by iteratively refining
  estimates of both parameters and latent variable distributions.
\item
  \textbf{Bayesian Methods}: Offer a principled way to incorporate prior
  knowledge, influencing learning outcomes especially in scenarios with
  limited data.
\end{itemize}
