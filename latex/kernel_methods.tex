\hypertarget{kernel-methods}{%
\section{16-Kernel Methods}\label{kernel-methods}}

\hypertarget{overview}{%
\subsection{Overview}\label{overview}}

Kernel methods enhance the expressiveness of linear models by embedding
data into high-dimensional feature spaces, enabling linear separation of
complex patterns. This chapter introduces the concept of embedding and
the kernel trick, facilitating computationally efficient learning in
high-dimensional spaces without explicitly computing the embeddings.

\hypertarget{key-concepts}{%
\subsection{Key Concepts}\label{key-concepts}}

\hypertarget{embedding-into-feature-spaces}{%
\subsubsection{Embedding into Feature
Spaces}\label{embedding-into-feature-spaces}}

\begin{itemize}
\item
  \textbf{Feature Space}: A higher-dimensional space where original data
  is mapped to enable complex pattern modeling.
\item
  \textbf{Mapping Function (\(\psi\))}: Transforms data from the
  original space to the feature space, potentially increasing its
  dimensionality and expressiveness.
\end{itemize}

\hypertarget{the-kernel-trick}{%
\subsubsection{The Kernel Trick}\label{the-kernel-trick}}

\begin{itemize}
\item
  \textbf{Kernel Function (K)}: Represents inner products in the feature
  space, allowing operations in high-dimensional spaces without explicit
  mappings.
\item
  \textbf{Gram Matrix}: An \(m \times m\) matrix containing the kernel
  evaluations between all pairs of training examples, central to
  kernel-based learning.
\end{itemize}

\hypertarget{polynomial-and-gaussian-kernels}{%
\subsubsection{Polynomial and Gaussian
Kernels}\label{polynomial-and-gaussian-kernels}}

\begin{itemize}
\item
  \textbf{Polynomial Kernel}: Allows learning polynomial decision
  boundaries in the original space. Defined as
  \(K(x, x') = (1 + \langle x, x' \rangle)^k\).
\item
  \textbf{Gaussian (RBF) Kernel}: Facilitates learning of non-linear
  boundaries by measuring similarity in terms of Euclidean distance.
  Defined as \(K(x, x') = e^{-\frac{\|x-x'\|^2}{2\sigma^2}}\).
\end{itemize}

\hypertarget{solving-svm-with-kernels}{%
\subsubsection{Solving SVM with
Kernels}\label{solving-svm-with-kernels}}

\begin{itemize}
\item
  \textbf{Soft-SVM with Kernels}: Extends the Soft-SVM formulation to
  feature spaces, optimized using the kernel trick to handle
  high-dimensional data efficiently.
\item
  \textbf{SGD for Kernelized SVM}: An adaptation of Stochastic Gradient
  Descent to optimize the SVM objective directly in the feature space
  using kernel functions.
\end{itemize}

\hypertarget{applications-and-prior-knowledge}{%
\subsubsection{Applications and Prior
Knowledge}\label{applications-and-prior-knowledge}}

\begin{itemize}
\item
  Kernels allow the incorporation of domain-specific prior knowledge
  through the choice of the mapping function or directly through the
  kernel function.
\item
  The kernel trick enables efficient computation and learning in
  potentially infinite-dimensional spaces, making it a powerful tool for
  a wide variety of learning tasks.
\end{itemize}

\hypertarget{important-formulas-and-theorems}{%
\subsection{Important Formulas and
Theorems}\label{important-formulas-and-theorems}}

\hypertarget{representer-theorem}{%
\subsubsection{Representer Theorem}\label{representer-theorem}}

Asserts that the optimal solution for regularized learning problems can
be represented as a linear combination of training examples in the
feature space.

\hypertarget{positive-semidefinite-requirement}{%
\subsubsection{Positive Semidefinite
Requirement}\label{positive-semidefinite-requirement}}

A symmetric function \(K : X \times X \to \mathbb{R}\) is a valid kernel
if and only if it is positive semidefinite, ensuring that it corresponds
to an inner product in some Hilbert space.
